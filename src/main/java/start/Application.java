package start;


import presentation.CustomerController;

public class Application {

    public static void main(String[] args) {
        CustomerController controller = new CustomerController();
        controller.start();
    }
}
