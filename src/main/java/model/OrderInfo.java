package model;

/**
 * Model class for additional order information
 * It basically contains each product for an order
 */
public class OrderInfo {
    private int id;
    private int quantity;
    private double totalPrice;
    private int orderId;
    private int productId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    /**
     *
     * @return A String that contains minimal information about the class
     */
    @Override
    public String toString() {
        return getId() + " " + getProductId() + " " + getQuantity() + " " + getTotalPrice() + " " + getOrderId() + "";
    }
}
