package dao;

import model.Customer;

/**
 * Class that extends the generic class which represents the Data Access Layer
 */
public class CustomerDAO extends AbstractDAO<Customer> {
    public CustomerDAO() {
        super();
    }
}
