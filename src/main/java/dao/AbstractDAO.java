package dao;
import connection.ConnectionFactory;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Generic class that is used for each table in the database to perform the CRUD operations
 * @param <T>
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    private final Class<T> type;

    /**
     * Constructor that sets the Class type
     */
    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Method used to create the select query through reflection
     * @param field The field used to filter data from the table
     * @return Returns the query as a String
     */
    private String createSelectQuery(String field) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT * FROM ");
        stringBuilder.append(type.getSimpleName().toLowerCase());
        stringBuilder.append(" WHERE " + field + " =?");
        return stringBuilder.toString();
    }

    /**
     * Method used to create the select query through reflection
     * @return Returns the query as a String
     */
    private String createSelectAllQuery() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT * FROM warehouse.");
        stringBuilder.append(type.getSimpleName().toLowerCase());
        return stringBuilder.toString();
    }

    /**
     * Method used to create the insert query through reflection
     * @param t The object to be inserted in the table
     * @return Returns the query as a String
     */
    private String createInsertQuery(T t) {
        Field[] fields = type.getDeclaredFields();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO warehouse.");
        stringBuilder.append(type.getSimpleName().toLowerCase());
        stringBuilder.append(" (");
        for (Field currentField : fields) {
            stringBuilder.append(currentField.getName().toLowerCase());
            stringBuilder.append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        stringBuilder.append(") VALUES (");
        try {
            for (Field currentField : fields) {
                currentField.setAccessible(true);
                Object value = currentField.get(t);
                stringBuilder.append("'" + value.toString() + "'");
                stringBuilder.append(",");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append(")");
        }
        catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, type.getName() + ". Couldn't access the fields. " + e.getMessage());
        }
        return stringBuilder.toString();
    }

    /**
     * Method used to create the update query through reflection
     * @param t The object to be updated in the table
     * @param field The field used to filter the data from the database
     * @return Returns the query as a String
     */
    private String createUpdateQuery(String field, T t) {
        Field[] fields = type.getDeclaredFields();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE ");
        stringBuilder.append(type.getSimpleName().toLowerCase());
        stringBuilder.append(" SET ");
        try {
            for (Field currentField : fields) {
                stringBuilder.append(currentField.getName().toLowerCase());
                stringBuilder.append(" = ");
                currentField.setAccessible(true);
                Object value = currentField.get(t);
                stringBuilder.append("'" + value.toString() + "'");
                stringBuilder.append(",");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append(" WHERE " + field + " =?");
        }
        catch (IllegalAccessException e) {
            LOGGER.log(Level.SEVERE, type.getName() + ". Couldn't access the fields. " + e.getMessage());
        }
        return stringBuilder.toString();
    }

    /**
     * Method used to create the delete query through reflection
     * @param field The field used to filter data from the table
     * @return Returns the query as a String
     */
    private String createDeleteQuery(String field) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DELETE FROM ");
        stringBuilder.append(type.getSimpleName().toLowerCase());
        stringBuilder.append(" WHERE " + field + " =?");
        return stringBuilder.toString();
    }

    /**
     * Method used to cast the result set to a list of objects through Reflection
     * @param resultSet The input result set to be transformed
     * @return The list of objects transformed from the result se
     */
    private List<T> createObjects(ResultSet resultSet) {
        List<T> objectsList = new ArrayList<>();
        try {
            while (resultSet.next())  {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                objectsList.add(instance);
            }
        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return objectsList;
    }

    /**
     * Method used to add a layer between the query and the objects in the program
     * @param id The field that helps identifying the object
     * @return The object resulted from the select query
     */
    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO: findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Method used to add a layer between the query and the objects in the program
     * @return The object resulted from the select query
     */
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO: findAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Method used to add a layer between the query and the objects in the program
     * @param t Object to be inserted
     */
    public void insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createInsertQuery(t);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO: insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * Method used to add a layer between the query and the objects in the program
     * @param t Object to be updated
     * @param id The field that helps identifying the object to be updated
     */
    public void update(T t, int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createUpdateQuery("id", t);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO: update " + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    /**
     * Method used to add a layer between the query and the objects in the program
     * @param id The field that helps identifying the object to be deleted
     */
    public void deleteById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO: delete" + e.getMessage());
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }
}
