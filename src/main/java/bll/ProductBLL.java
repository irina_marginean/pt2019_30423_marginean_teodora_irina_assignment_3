package bll;

import bll.validators.PriceValidator;
import bll.validators.StockValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ProductBLL {
    private List<Validator<Product>> validators;

    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new PriceValidator());
        validators.add(new StockValidator());
    }

    public Product findById(int id) {
        ProductDAO productDAO = new ProductDAO();
        Product product = productDAO.findById(id);
        if (product == null) {
            throw new NoSuchElementException("The product with the id" +
                    id + "was not found");
        }
        return product;
    }

    public void insert(Product product) {
        try {
            for (Validator<Product> validator : validators) {
                validator.validate(product);
            }
            ProductDAO productDAO = new ProductDAO();
            productDAO.insert(product);
        }
        catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public List<Product> findAll() {
        ProductDAO productDAO = new ProductDAO();
        List<Product> products = productDAO.findAll();
        if (products == null) {
            throw new NoSuchElementException("No products found");
        }
        return products;
    }

    public void edit(Product product, int id) {
        try {
            ProductDAO productDAO = new ProductDAO();
            for (Validator<Product> validator : validators) {
                validator.validate(product);
            }
            productDAO.update(product, id);
        }
        catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public void delete(Product product) {
        ProductDAO productDAO = new ProductDAO();
        productDAO.deleteById(product.getId());
    }

    public void deleteById(int id) {
        ProductDAO productDAO = new ProductDAO();
        productDAO.deleteById(id);
    }
}
