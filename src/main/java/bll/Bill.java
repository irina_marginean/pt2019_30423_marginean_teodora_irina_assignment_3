package bll;

import model.Customer;
import model.Order;
import model.OrderInfo;
import model.Product;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Class that is used to manage the placing of orders
 */
public class Bill {
    public static int orderId = 0;
    public static int orderInfoId = 0;
    private Order order;
    private OrderBLL orderBLL;
    private OrderInfoBLL orderInfoBLL;

    public Bill() {
        orderBLL = new OrderBLL();
        orderInfoBLL = new OrderInfoBLL();
        List<Order> orders = orderBLL.findAll();
        orderId = orders.get(orders.size() - 1).getId();
        order = new Order();
        orderId++;
        order.setId(orderId);
        orderInfoId = orderId + 20;
    }

    /**
     * Creates the text file equivalent to a bill
     * @param order The main order that has only 1 client and multiple products
     * @param orderInfos It is basically the list of products in an order
     * @param total Total price of an order
     */
    public void createBillText(Order order, List<OrderInfo> orderInfos, double total) {
        try {
            ProductBLL productBLL = new ProductBLL();
            CustomerBLL customerBLL = new CustomerBLL();
            PrintWriter writer = new PrintWriter("order_" + orderId + ".txt", "UTF-8");
            writer.println("Order number: " + orderId);
            writer.println();
            Customer customer = customerBLL.findById(order.getCustomerId());
            writer.println("Client id: " + customer.getId());
            writer.println("Client name: " + customer.getName());
            writer.println("Client address: " + customer.getAddress());
            writer.println("Client e-mail: " + customer.getEmail());
            writer.println("Client phone number: " + customer.getPhone());
            writer.println("\n");
            writer.println("Products: ");
            writer.println();
            for (OrderInfo orderInfo : orderInfos) {
                Product product = productBLL.findById(orderInfo.getProductId());
                writer.println("Product: " + product.getName());
                writer.println("\t" + "Price: " +  product.getPrice());
                writer.println("\t" + "Quantity: " +  orderInfo.getQuantity());
                writer.println(("\t\t" + "Total price per product: " + orderInfo.getTotalPrice()));
                writer.println();
            }
            writer.println("\n");
            writer.println("Total price: " + total);
            writer.close();
        }
        catch (IOException e) {
            System.out.println("Couldn't create the file" + "order_" + orderId + ".txt");
        }
    }

    /**
     * Places an order, by inserting it in the database and creating the bill
     * @param customer Customer that placed the order
     * @param orderInfos The products oredered
     * @param total total Price of the order
     */
    public void placeOrder(Customer customer, List<OrderInfo> orderInfos, double total) {
        order.setCustomerId(customer.getId());
        order.setTotal(total);
        orderBLL.insert(order);
        for (OrderInfo orderInfo : orderInfos) {
            total += orderInfo.getTotalPrice();
            orderInfoBLL.insert(orderInfo);
        }
        createBillText(order, orderInfos, total);
    }

    /**
     * Adds a product to the order
     * @param product Product to be ordered
     * @param quantity Quantity of product to be ordered
     * @param orderId The id of the order
     * @return The Information about the product to be ordered
     * @throws IllegalStateException if understock is encountered
     */
    public OrderInfo selectProduct(Product product, int quantity, int orderId) throws IllegalStateException {
        OrderInfo orderInfo = new OrderInfo();
        ProductBLL productBLL = new ProductBLL();
        orderInfoId++;
        orderInfo.setId(orderInfoId);
        orderInfo.setOrderId(orderId);
        orderInfo.setProductId(product.getId());
        orderInfo.setQuantity(quantity);
        orderInfo.setTotalPrice(product.getPrice() * quantity);
        if (orderInfo.getQuantity() <= product.getQuantity()) {
            product.setQuantity(product.getQuantity() - orderInfo.getQuantity());
            productBLL.edit(product, product.getId());
            return  orderInfo;
        }
        else {
            throw new IllegalStateException("Not enough stock");
        }
    }

    public Order getOrder() {
        return order;
    }
}
