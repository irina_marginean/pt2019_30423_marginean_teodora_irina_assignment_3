package bll;

import dao.OrderDAO;
import model.Order;

import java.util.List;
import java.util.NoSuchElementException;

public class OrderBLL  {
    public void insert(Order order) {
        OrderDAO orderDAO = new OrderDAO();
        orderDAO.insert(order);
    }

    public List<Order> findAll() {
        OrderDAO orderDAO = new OrderDAO();
        List<Order> orders = orderDAO.findAll();
        if (orders == null) {
            throw new NoSuchElementException("No products found");
        }
        return orders;
    }
}
