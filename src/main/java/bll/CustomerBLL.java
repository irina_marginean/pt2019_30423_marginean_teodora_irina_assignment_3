package bll;

import bll.validators.EmailValidator;
import bll.validators.PhoneNumberValidator;
import bll.validators.Validator;
import dao.CustomerDAO;
import model.Customer;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Business Logic Layer for the customer that contains the validators
 */
public class CustomerBLL {
    private List<Validator<Customer>> validators;

    public CustomerBLL() {
        validators = new ArrayList<Validator<Customer>>();
        validators.add(new EmailValidator());
        validators.add(new PhoneNumberValidator());
    }

    public Customer findById(int id) {
        CustomerDAO customerDAO = new CustomerDAO();
        Customer customer = customerDAO.findById(id);
        if (customer == null) {
            throw new NoSuchElementException("The customer with the id" +
                    id + "was not found");
        }
        return customer;
    }

    public void insert(Customer customer) {
        try {
            for (Validator<Customer> validator : validators) {
                validator.validate(customer);
            }
            CustomerDAO customerDAO = new CustomerDAO();
            customerDAO.insert(customer);
        }
         catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public List<Customer> findAll() {
        CustomerDAO customerDAO = new CustomerDAO();
        List<Customer> customers = customerDAO.findAll();
        if (customers == null) {
            throw new NoSuchElementException("No customers found");
        }
        return customers;
    }

    public void edit(Customer customer, int id) {
        try {
            CustomerDAO customerDAO = new CustomerDAO();
            for (Validator<Customer> validator : validators) {
                validator.validate(customer);
            }
            customerDAO.update(customer, id);
        }
        catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public void delete(Customer customer) {
        CustomerDAO customerDAO = new CustomerDAO();
        customerDAO.deleteById(customer.getId());
    }

    public void deleteById(int id) {
        CustomerDAO customerDAO = new CustomerDAO();
        customerDAO.deleteById(id);
    }
}
