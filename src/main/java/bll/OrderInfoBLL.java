package bll;

import dao.OrderInfoDAO;
import model.OrderInfo;

import java.util.List;
import java.util.NoSuchElementException;

public class OrderInfoBLL {
    public void insert(OrderInfo orderInfo) {
        OrderInfoDAO orderInfoDAO = new OrderInfoDAO();
        orderInfoDAO.insert(orderInfo);
    }

    public List<OrderInfo> findAll() {
        OrderInfoDAO orderInfoDAO = new OrderInfoDAO();
        List<OrderInfo> orderInfos = orderInfoDAO.findAll();
        if (orderInfos == null) {
            throw new NoSuchElementException("No products found");
        }
        return orderInfos;
    }
}
