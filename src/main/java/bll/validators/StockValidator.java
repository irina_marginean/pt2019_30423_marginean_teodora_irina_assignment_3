package bll.validators;

import model.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class StockValidator implements Validator<Product> {
    @Override
    public void validate(Product product) {
        if (product.getQuantity() < 0) {
            throw new IllegalArgumentException("Quantity cannot be a negative value");
        }
    }
}
