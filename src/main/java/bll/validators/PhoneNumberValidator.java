package bll.validators;

import model.Customer;

import java.util.regex.Pattern;

public class PhoneNumberValidator implements Validator<Customer> {
    @Override
    public void validate(Customer customer) {
        Pattern pattern = Pattern.compile("(0/0040)?[7-9][0-9]{8}");
        if (!pattern.matcher(customer.getPhone()).matches()) {
            throw new IllegalArgumentException("Phone number is not valid!");
        }
    }
}
