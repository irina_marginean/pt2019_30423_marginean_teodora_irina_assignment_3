package bll.validators;

import model.Product;

public class PriceValidator implements Validator<Product> {
    @Override
    public void validate(Product product) {
        if (product.getQuantity() < 0) {
            throw new IllegalArgumentException("Price cannot be a negative value");
        }
    }
}
