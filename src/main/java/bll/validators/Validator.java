package bll.validators;

/**
 * Generic class used to validate fields in objects
 * @param <T>
 */
public interface Validator<T> {
    public void validate(T t);
}
