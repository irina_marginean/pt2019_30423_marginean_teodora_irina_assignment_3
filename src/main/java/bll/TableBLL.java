package bll;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.lang.reflect.Field;
import java.util.List;

/**
 * Generic class that is used to construct a JTable
 * @param <T>
 */
public class TableBLL<T> {

    /**
     * Creates a JTable through Reflection
     * @param objects The list of objects to be added to the table
     * @param type The Class type
     * @return The created JTable
     */
    public JTable createTable(List<T> objects, Class<T> type) {
        JTable table = new JTable();
        Field[] fields = type.getDeclaredFields();
        DefaultTableModel model = new DefaultTableModel();
        Object[] columnsName = new Object[type.getDeclaredFields().length];
        int i = 0;
        for (Field currentField : fields) {
            columnsName[i] = currentField.getName();
            i++;
        }
        model.setColumnIdentifiers(columnsName);
        Object[] rowData = new Object[type.getDeclaredFields().length];
        for (T currentT : objects) {
            i = 0;
            for (Field currentField : fields) {
                currentField.setAccessible(true);
                try {
                    rowData[i] = currentField.get(currentT).toString();
                    i++;
                }
                catch (IllegalAccessException e) { e.printStackTrace(); }
            }
            model.addRow(rowData);
        }
        table.setModel(model);
        return table;
    }
}
