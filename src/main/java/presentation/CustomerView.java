/*
 * Created by JFormDesigner on Mon May 06 04:41:54 EEST 2019
 */

package presentation;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author Irina Marginean
 */
public class CustomerView extends JFrame {
    public CustomerView(JTable table) {
        initComponents(table);
    }

    public void setCustomerTable(JTable table) {
        customerTable = table;
    }

    private void initComponents(JTable table) {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Irina Marginean
        scrollPane1 = new JScrollPane();
        customerTable = table;
        addButton = new JButton();
        editButton = new JButton();
        deleteButton = new JButton();
        id = new JTextField();
        name = new JTextField();
        address = new JTextField();
        email = new JTextField();
        phoneNumber = new JTextField();
        label1 = new JLabel();
        label2 = new JLabel();
        label3 = new JLabel();
        label4 = new JLabel();
        label5 = new JLabel();
        idToUpdate = new JTextField();
        label6 = new JLabel();
        goToProductsButton = new JButton();

        //======== this ========
        setTitle("Customers");
        Container contentPane = getContentPane();

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(customerTable);
        }

        //---- addButton ----
        addButton.setText("Add ");

        //---- editButton ----
        editButton.setText("Edit");

        //---- deleteButton ----
        deleteButton.setText("Delete");

        //---- label1 ----
        label1.setText("Id");

        //---- label2 ----
        label2.setText("Name");

        //---- label3 ----
        label3.setText("E-mail");

        //---- label4 ----
        label4.setText("Phone number");

        //---- label5 ----
        label5.setText("Address");

        //---- label6 ----
        label6.setText("Id of customer to edit");

        //---- goToProductsButton ----
        goToProductsButton.setText("Go to products");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addComponent(scrollPane1)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(label1, GroupLayout.PREFERRED_SIZE, 61, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(id, GroupLayout.PREFERRED_SIZE, 126, GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(label2)
                                        .addComponent(name, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)))
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                    .addGap(0, 0, Short.MAX_VALUE)
                                    .addComponent(addButton, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                                    .addGap(53, 53, 53)))
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addGroup(contentPaneLayout.createParallelGroup()
                                    .addGroup(contentPaneLayout.createSequentialGroup()
                                        .addGap(29, 29, 29)
                                        .addGroup(contentPaneLayout.createParallelGroup()
                                            .addComponent(address, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label5))
                                        .addGap(33, 33, 33)
                                        .addGroup(contentPaneLayout.createParallelGroup()
                                            .addComponent(email, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
                                            .addComponent(label3)))
                                    .addGroup(contentPaneLayout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addGroup(contentPaneLayout.createParallelGroup()
                                            .addComponent(idToUpdate, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                                            .addGroup(contentPaneLayout.createSequentialGroup()
                                                .addComponent(editButton, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
                                                .addGap(106, 106, 106)
                                                .addComponent(deleteButton, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)))))
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addComponent(label6)
                                    .addGap(185, 185, 185)))
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGap(29, 29, 29)
                                    .addGroup(contentPaneLayout.createParallelGroup()
                                        .addComponent(phoneNumber)
                                        .addGroup(contentPaneLayout.createSequentialGroup()
                                            .addComponent(label4)
                                            .addGap(0, 55, Short.MAX_VALUE))))
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                                    .addComponent(goToProductsButton)
                                    .addGap(10, 10, 10)))))
                    .addContainerGap())
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(19, 19, 19)
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(label1)
                        .addComponent(label2)
                        .addComponent(label5)
                        .addComponent(label3)
                        .addComponent(label4))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(address, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(email, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(phoneNumber, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(id, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(name, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                            .addComponent(label6)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(idToUpdate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(editButton)
                                .addComponent(addButton)
                                .addComponent(deleteButton))
                            .addGap(25, 25, 25))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGap(18, 18, 18)
                            .addComponent(goToProductsButton, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(34, Short.MAX_VALUE))))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Irina Marginean
    private JScrollPane scrollPane1;
    private JTable customerTable;
    private JButton addButton;
    private JButton editButton;
    private JButton deleteButton;
    private JTextField id;
    private JTextField name;
    private JTextField address;
    private JTextField email;
    private JTextField phoneNumber;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JLabel label5;
    private JTextField idToUpdate;
    private JLabel label6;
    private JButton goToProductsButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    protected void addAddButtonListener(final ActionListener actionListener) {
        addButton.addActionListener(actionListener);
    }

    protected void addEditButtonListener(final ActionListener actionListener) {
        editButton.addActionListener(actionListener);
    }

    protected void addDeleteButtonListener(final ActionListener actionListener) {
        deleteButton.addActionListener(actionListener);
    }

    protected void addGoToProductsButtonListener(final ActionListener actionListener) {
        goToProductsButton.addActionListener(actionListener);
    }

    public void showWarningMessage(String message) {
        JOptionPane.showMessageDialog(getContentPane(), message);
    }

    public String getId() { return id.getText(); }

    public String getName() { return name.getText(); }

    public String getAddress() { return address.getText(); }

    public String getQEmail() { return email.getText(); }

    public String getPhoneNumber() { return phoneNumber.getText(); }

    public String getIdToUpdate() { return idToUpdate.getText(); }

}
