package presentation;

import bll.ProductBLL;
import bll.TableBLL;
import model.Product;

import javax.swing.*;
import java.util.List;

/**
 * Controller class for the Customer Window
 */
public class ProductController {
    private ProductView productView;
    private ProductBLL productBLL;

    /**
     * A method that behaves like a constructor,
     * except that is called multiple times in this class to reset the controller
     */
    public void start() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
            System.out.println("Error");
        }
        TableBLL<Product> productTable = new TableBLL<>();
        productBLL = new ProductBLL();
        List<Product> products = productBLL.findAll();
        productView = new ProductView(productTable.createTable(products, Product.class));
        initializeButtonListeners();
        productView.setVisible(true);
    }

    /**
     * Private method that overrides the events for components int the ProductView Class
     */
    private void initializeButtonListeners() {
        productView.addAddButtonListener(e -> {
            try {
                Product product = new Product();
                validateProduct(product);
                try {
                    productBLL.insert(product);
                    productView.dispose();
                    start();
                }
                catch (NumberFormatException ex) {
                    productView.showWarningMessage("Id of product to update is not valid");
                }
            }
            catch (IllegalArgumentException ex) {
                productView.showWarningMessage(ex.getMessage());
            }
        });

        productView.addEditButtonListener(e -> {
            try {
                Product product = new Product();
                validateProduct(product);
                try {
                    productBLL.edit(product, Integer.parseInt(productView.getIdToUpdate()));
                    productView.dispose();
                    start();
                }
                catch (NumberFormatException ex) {
                    productView.showWarningMessage("Id of product to update is not valid");
                }
            }
            catch (IllegalArgumentException ex) {
                productView.showWarningMessage(ex.getMessage());
            }
        });

        productView.addDeleteButtonListener(e -> {
            int id = Integer.parseInt(productView.getId());
            productBLL.deleteById(id);
            productView.dispose();
            start();
        });

        productView.addGoToOrdersButtonListener(e -> {
            productView.dispose();
            OrderController orderController = new OrderController();
            orderController.start();
        });
    }

    /**
     * Method that validates a product fromt he user's input
     * @param product Product to be validated
     */
    private void validateProduct(Product product) {
        try {
            product.setId(Integer.parseInt(productView.getId()));
            product.setName(productView.getName());
        }
        catch (NumberFormatException ex) {
            productView.showWarningMessage("Id is not valid");
        }
        try {
            product.setPrice(Double.parseDouble(productView.getPrice()));
        }
        catch (NumberFormatException ex) {
            productView.showWarningMessage("Price input is not valid");
        }
        try {
            product.setQuantity(Integer.parseInt(productView.getQuantity()));
        }
        catch (NumberFormatException ex) {
            productView.showWarningMessage("Quantity input is not valid");
        }
    }
}
