package presentation;

import bll.Bill;
import bll.CustomerBLL;
import bll.ProductBLL;
import model.Customer;
import model.Order;
import model.OrderInfo;
import model.Product;
import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller class for the Order Window
 */
public class OrderController {
    private OrderView orderView;
    private CustomerBLL customerBLL;
    private ProductBLL productBLL;
    private double totalPrice = 0;
    private JComboBox<Product> productsComboBox;
    private JComboBox<Customer> customersComboBox;
    private Order currentOrder;
    private List<OrderInfo> orderInfos;
    private Bill bill;

    /**
     * A method that behaves like a constructor,
     * except that is called multiple times in this class to reset the controller
     */
    public void start() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
            System.out.println("Error");
        }
        customerBLL = new CustomerBLL();
        productBLL = new ProductBLL();
        orderInfos = new ArrayList<>();
        bill = new Bill();
        productsComboBox = new JComboBox();
        customersComboBox = new JComboBox();
        List<Customer> customerList = customerBLL.findAll();
        List<Product> productList = productBLL.findAll();
        currentOrder = bill.getOrder();
        for (Customer customer : customerList) {
            customersComboBox.addItem(customer);
        }
        for (Product product : productList) {
            productsComboBox.addItem(product);
        }
        orderView = new OrderView(productsComboBox, customersComboBox);
        initializeButtonListeners();
        orderView.setVisible(true);
    }

    /**
     * Private method that overrides the events for components int the OrderView Class
     */
    private void initializeButtonListeners() {
        orderView.addAddToOrderButtonListener(e -> {
            try {
                OrderInfo orderInfo;
                int quantity = Integer.parseInt(orderView.getQuantity());
                orderInfo = bill.selectProduct(orderView.getSelectedProduct(e), quantity, currentOrder.getId());
                totalPrice += orderInfo.getTotalPrice();
                orderView.setCurrentTotalField(totalPrice);
                orderInfos.add(orderInfo);
            }
            catch (IllegalStateException ex) {
                orderView.showWarningMessage("Not enough stock");
            }
        });

        orderView.addFinalizeOrderButtonListener(e -> {
            bill.placeOrder(orderView.getSelectedCustomer(e), orderInfos, totalPrice);
            bill = null;
        });

        orderView.addGoToCustomersButtonListener(e -> {
            orderView.dispose();
            CustomerController customerController = new CustomerController();
            customerController.start();
        });
    }
}
