/*
 * Created by JFormDesigner on Tue May 07 01:17:17 EEST 2019
 */

package presentation;

import model.Customer;
import model.Product;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author Irina Marginean
 */
public class OrderView extends JFrame {
    public OrderView(JComboBox products, JComboBox customers) {
        initComponents(products, customers);
    }

    private void initComponents(JComboBox products, JComboBox customers) {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Irina Marginean
        scrollPane1 = new JScrollPane();
        customersComboBox = customers;
        productsComboBox = products;
        goToCustomersButton = new JButton();
        label1 = new JLabel();
        label2 = new JLabel();
        quantityField = new JTextField();
        label3 = new JLabel();
        currentTotalField = new JTextField();
        label4 = new JLabel();
        addToOrderButton = new JButton();
        button1 = new JButton();

        //======== this ========
        setTitle("Orders");
        Container contentPane = getContentPane();

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(customersComboBox);
        }

        //---- goToCustomersButton ----
        goToCustomersButton.setText("Go to customers");

        //---- label1 ----
        label1.setText("Select a customer");

        //---- label2 ----
        label2.setText("Select a product");

        //---- label3 ----
        label3.setText("Select a quantity");

        //---- label4 ----
        label4.setText("Current total");

        //---- addToOrderButton ----
        addToOrderButton.setText("Add to order");

        //---- button1 ----
        button1.setText("Finalize order");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(39, 39, 39)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(label2)
                            .addContainerGap(548, Short.MAX_VALUE))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(label3)
                                .addComponent(scrollPane1, GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE)
                                .addComponent(label1)
                                .addComponent(productsComboBox)
                                .addComponent(quantityField)
                                .addComponent(label4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(currentTotalField)
                                .addComponent(addToOrderButton, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE)
                                .addComponent(button1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 159, Short.MAX_VALUE)
                            .addComponent(goToCustomersButton)
                            .addGap(40, 40, 40))))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addContainerGap(328, Short.MAX_VALUE)
                            .addComponent(goToCustomersButton, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGap(29, 29, 29)
                            .addComponent(label1)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(label2)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(productsComboBox, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                            .addGap(21, 21, 21)
                            .addComponent(label3)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(quantityField, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                            .addComponent(label4)
                            .addGap(18, 18, 18)
                            .addComponent(currentTotalField, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(addToOrderButton, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(button1, GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)))
                    .addGap(20, 20, 20))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Irina Marginean
    private JScrollPane scrollPane1;
    private JComboBox customersComboBox;
    private JComboBox productsComboBox;
    private JButton goToCustomersButton;
    private JLabel label1;
    private JLabel label2;
    private JTextField quantityField;
    private JLabel label3;
    private JTextField currentTotalField;
    private JLabel label4;
    private JButton addToOrderButton;
    private JButton button1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    protected void addGoToCustomersButtonListener(final ActionListener actionListener) {
        goToCustomersButton.addActionListener(actionListener);
    }

    protected void addAddToOrderButtonListener(final ActionListener actionListener) {
        addToOrderButton.addActionListener(actionListener);
    }


    protected void addFinalizeOrderButtonListener(final ActionListener actionListener) {
        button1.addActionListener(actionListener);
    }

    protected String getQuantity() {
        return quantityField.getText();
    }

    protected void setCurrentTotalField(double total) {
        currentTotalField.setText(total + "");
    }

    protected void showWarningMessage(String message) {
        JOptionPane.showMessageDialog(getContentPane(), message);
    }

    protected Product getSelectedProduct(ActionEvent evt) {
        if (productsComboBox.getSelectedItem() != null) {
            return (Product) productsComboBox.getSelectedItem();
        }
        return null;
    }

    protected Customer getSelectedCustomer(ActionEvent evt) {
        if (customersComboBox.getSelectedItem() != null) {
            return (Customer) customersComboBox.getSelectedItem();
        }
        return null;
    }
}
