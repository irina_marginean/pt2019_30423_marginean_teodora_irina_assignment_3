package presentation;

import bll.CustomerBLL;
import bll.TableBLL;
import model.Customer;
import javax.swing.*;
import java.util.List;

/**
 * Controller class for the Customer Window
 */
public class CustomerController {
    private CustomerView customerView;
    private CustomerBLL customerBLL;

    /**
     * A method that behaves like a constructor,
     * except that is called multiple times in this class to reset the controller
     */
    public void start() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) {
            System.out.println("Error");
        }
        TableBLL<Customer> customerTable = new TableBLL<>();
        customerBLL = new CustomerBLL();
        List<Customer> customers = customerBLL.findAll();
        customerView = new CustomerView(customerTable.createTable(customers, Customer.class));
        initializeButtonListeners();
        customerView.setVisible(true);
    }

    /**
     * Private method that overrides the events for components int the CustomerView Class
     */
    private void initializeButtonListeners() {
        customerView.addAddButtonListener(e -> {
            try {
                Customer customer = new Customer();
                customer.setId(Integer.parseInt(customerView.getId()));
                customer.setName(customerView.getName());
                customer.setAddress(customerView.getAddress());
                customer.setEmail(customerView.getQEmail());
                customer.setPhone(customerView.getPhoneNumber());
                customerBLL.insert(customer);
                customerView.dispose();
                start();
            }
            catch (NumberFormatException nfEx) {
                customerView.showWarningMessage("Input for id is not valid!");
            }
            catch(IllegalArgumentException ex) {
                customerView.showWarningMessage(ex.getMessage());
            }
        });

        customerView.addEditButtonListener(e -> {
            try {
                Customer customer = new Customer();
                customer.setId(Integer.parseInt(customerView.getId()));
                customer.setName(customerView.getName());
                customer.setAddress(customerView.getAddress());
                customer.setEmail(customerView.getQEmail());
                customer.setPhone(customerView.getPhoneNumber());
                customerBLL.edit(customer, Integer.parseInt(customerView.getIdToUpdate()));
                customerView.dispose();
                start();
            }
            catch (NumberFormatException nfEx) {
                customerView.showWarningMessage("Input for id is not valid!");
            }
            catch(IllegalArgumentException ex) {
                customerView.showWarningMessage(ex.getMessage());
            }
        });

        customerView.addDeleteButtonListener(e -> {
            int id = Integer.parseInt(customerView.getId());
            customerBLL.deleteById(id);
            customerView.dispose();
            start();
        });

        customerView.addGoToProductsButtonListener(e -> {
            customerView.dispose();
            ProductController productController = new ProductController();
            productController.start();
        });
    }

}
