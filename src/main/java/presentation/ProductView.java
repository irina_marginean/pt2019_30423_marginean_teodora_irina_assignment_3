/*
 * Created by JFormDesigner on Mon May 06 15:33:54 EEST 2019
 */

package presentation;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.GroupLayout;

/**
 * @author Irina Marginean
 */
public class ProductView extends JFrame {
    public ProductView(JTable table) {
        initComponents(table);
    }

    public void setTable(JTable table) {
        productTable = table;
    }

    private void initComponents(JTable table) {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Irina Marginean
        scrollPane1 = new JScrollPane();
        productTable = table;
        idFiels = new JTextField();
        nameField = new JTextField();
        addButton = new JButton();
        editButton = new JButton();
        deleteButton = new JButton();
        priceField = new JTextField();
        quantityFiels = new JTextField();
        idLabel = new JLabel();
        nameLabel = new JLabel();
        priceLabel = new JLabel();
        quantityLabel = new JLabel();
        idToUpdate = new JTextField();
        label5 = new JLabel();
        goToOrders = new JButton();

        //======== this ========
        setTitle("Products");
        Container contentPane = getContentPane();

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(productTable);
        }

        //---- addButton ----
        addButton.setText("Add");

        //---- editButton ----
        editButton.setText("Edit");

        //---- deleteButton ----
        deleteButton.setText("Delete");

        //---- idLabel ----
        idLabel.setText("Id");

        //---- nameLabel ----
        nameLabel.setText("Name");

        //---- priceLabel ----
        priceLabel.setText("Price");

        //---- quantityLabel ----
        quantityLabel.setText("Quantity");

        //---- label5 ----
        label5.setText("Id of product to update");

        //---- goToOrders ----
        goToOrders.setText("Go to Orders");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addComponent(scrollPane1)
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addGroup(GroupLayout.Alignment.LEADING, contentPaneLayout.createSequentialGroup()
                                    .addComponent(idLabel)
                                    .addGap(148, 148, 148)
                                    .addComponent(nameLabel)
                                    .addGap(127, 127, 127)
                                    .addComponent(priceLabel)
                                    .addGap(0, 0, Short.MAX_VALUE))
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGap(57, 57, 57)
                                    .addComponent(addButton, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                                    .addComponent(editButton, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGap(0, 235, Short.MAX_VALUE)
                                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(idToUpdate)
                                        .addComponent(label5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addGroup(contentPaneLayout.createParallelGroup()
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(deleteButton, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
                                    .addGap(41, 41, 41)
                                    .addComponent(goToOrders)
                                    .addGap(17, 17, 17))
                                .addGroup(contentPaneLayout.createSequentialGroup()
                                    .addGap(118, 118, 118)
                                    .addComponent(quantityLabel)
                                    .addGap(141, 141, 141))))
                        .addGroup(GroupLayout.Alignment.TRAILING, contentPaneLayout.createSequentialGroup()
                            .addComponent(idFiels, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nameField, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
                            .addGap(40, 40, 40)
                            .addComponent(priceField, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
                            .addGap(59, 59, 59)
                            .addComponent(quantityFiels, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                            .addGap(72, 72, 72))))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 238, GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(quantityLabel)
                        .addComponent(priceLabel)
                        .addComponent(nameLabel)
                        .addComponent(idLabel))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(idFiels, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(quantityFiels, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(priceField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(nameField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(label5)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(idToUpdate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(deleteButton)
                                .addComponent(editButton)
                                .addComponent(addButton)))
                        .addComponent(goToOrders, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Irina Marginean
    private JScrollPane scrollPane1;
    private JTable productTable;
    private JTextField idFiels;
    private JTextField nameField;
    private JButton addButton;
    private JButton editButton;
    private JButton deleteButton;
    private JTextField priceField;
    private JTextField quantityFiels;
    private JLabel idLabel;
    private JLabel nameLabel;
    private JLabel priceLabel;
    private JLabel quantityLabel;
    private JTextField idToUpdate;
    private JLabel label5;
    private JButton goToOrders;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    protected void addAddButtonListener(final ActionListener actionListener) {
        addButton.addActionListener(actionListener);
    }

    protected void addEditButtonListener(final ActionListener actionListener) {
        editButton.addActionListener(actionListener);
    }

    protected void addDeleteButtonListener(final ActionListener actionListener) {
        deleteButton.addActionListener(actionListener);
    }

    protected void addGoToOrdersButtonListener(final ActionListener actionListener) {
        goToOrders.addActionListener(actionListener);
    }

    public void showWarningMessage(String message) {
        JOptionPane.showMessageDialog(getContentPane(), message);
    }

    public String getId() { return idFiels.getText(); }

    public String getName() { return nameField.getText(); }

    public String getPrice() { return priceField.getText(); }

    public String getQuantity() { return quantityFiels.getText(); }

    public String getIdToUpdate() { return idToUpdate.getText(); }
}
